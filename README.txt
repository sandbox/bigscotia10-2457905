Install like you usually would. (https://www.drupal.org/documentation/install/modules-themes/modules-7)

Admin settings are all found in: 
Configuration > Web services > Yammer (example.com/admin/config/services/yammer).

All functionality is separated into blocks.
If you'd like, you can include the rating module, it adds that to the Yammer feed object if there is rating value for that block bundle.

**REMOVED, NO LONGER MAINTAINED**
